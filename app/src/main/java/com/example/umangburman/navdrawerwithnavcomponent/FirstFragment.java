package com.example.umangburman.navdrawerwithnavcomponent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.umangburman.navdrawerwithnavcomponent.binding.UserModel;
import com.example.umangburman.navdrawerwithnavcomponent.databinding.FirstFragmentBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

public class FirstFragment extends Fragment {


    private FirstFragmentBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.first_fragment, container, false);
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setBindingData();
    }

    private void setBindingData() {
        final UserModel model = new UserModel("Vidyadhar Reddy", null);
        binding.setUserModel(model);


        binding.btnNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirstFragmentDirections.ActionToNextActivity navDirections = FirstFragmentDirections.actionToNextActivity(model);
                Navigation.findNavController(v).navigate(navDirections);
            }
        });

    }

}
