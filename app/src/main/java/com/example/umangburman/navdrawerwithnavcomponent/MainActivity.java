package com.example.umangburman.navdrawerwithnavcomponent;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.umangburman.navdrawerwithnavcomponent.databinding.ActivityMainBinding;
import com.google.android.material.navigation.NavigationView;

import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    public NavController navController;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setupNavigation();

    }

    private void setupNavigation() {

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        Set<Integer> mList = new TreeSet<>();
        mList.add(R.id.firstFragment);
        mList.add(R.id.secondFragment);
        mList.add(R.id.thirdFragment);

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(mList)
                .setDrawerLayout(binding.drawerLayout)
                .build();


        NavigationUI.setupActionBarWithNavController(this, navController);
        NavigationUI.setupWithNavController(binding.toolbar, navController, appBarConfiguration);


        binding.navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public boolean onSupportNavigateUp() {
        return navController.navigateUp() || super.onSupportNavigateUp();
        //return NavigationUI.navigateUp(navController, mainActivityBinding.drawerLayout);
    }


    @Override
    public void onBackPressed() {

        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START))
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        menuItem.setChecked(true);
        binding.drawerLayout.closeDrawers();

        int id = menuItem.getItemId();
        int currentId = Objects.requireNonNull(navController.getCurrentDestination()).getId();
        switch (id) {
            case R.id.first:
                if (currentId != R.id.firstFragment)
                    navController.navigate(R.id.firstFragment);
                break;
            case R.id.second:
                if (currentId != R.id.secondFragment)
                    navController.navigate(R.id.secondFragment);
                break;
            case R.id.third:
                if (currentId != R.id.thirdFragment)
                    navController.navigate(R.id.thirdFragment);
                break;

        }
        return true;

    }
}
